"use strict";

const loadURL = require('./modeles/getURL').loadURL;
const parser = require('./modeles/parser');
const mongoUtil = require('./db');


const baseUrl = 'http://directors.dundb.co.il'

const companyTableSelector = 'table.gridStyleCompanies > tbody > tr:not(.gridStyleCompanies_header)'


const run = async () => {
    const items = await db.collection('names')
        .find({lastChecked: {$exists: false}})
        .limit(3)
        .toArray();
    console.log('items length: ', items.length);
    const funcs = items.forEach(name => parseNameData(name));
    const results = Promise.all(funcs);

    results.then(data => {
            console.log('data: ', data);
            run()
        });
}


const parseNameData = async (name) => {
    let html;
    const url = baseUrl + name.url;

    try {
        html = await loadURL(url);
        console.log('loading: ', name.name, name.url);
    } catch (e) {
        console.log('error: ', e);
        throw e;
    }

    await buildBoardsList(html, name);
}


const buildBoardsList = async (html, name) => {
    const rows = await parser.testSelector(companyTableSelector, html);
    let type;
    const promises = [];
    rows.each(async (i, row) => {
        if (row.children.length === 3) {
            type = row.children[1].children[0].data;
        } else {
            const promise = buildDataBlock(row, type);
            promises.push(promise);
        }
    });
    Promise.all(promises)
        .then((data) => {
            console.log('data: ', data);
            updateBoards(data, name);
        })
        .catch(e => {
            throw new Error(e)
        });
}

const buildDataBlock = async (row, type) => {
    const tds = await parser.testSelector('td', row);
    try {
        return await parser.getDirectorBoards(tds, type);
    } catch (e) {
        throw new Error('Getboards error: ' + e);
    }
}


const updateBoards = async (companiesArr, name) => {
    companiesArr.forEach(c => {
        console.log('c:', c)
        name.title = c.title;
        delete c.title;
        delete name.url;
        db.collection('boards')
            .updateOne(
                {"companyID": c.companyID},
                {$set: c, $addToSet: {'names': name}},
                {upsert: true}, async (err, results) => {
                    if (err) throw err;
                    console.log('Company updated: ', c.name);
                    return results;
                })
    });
    await updateName(companiesArr, name);
    return companiesArr.length;
}


async function updateName(companiesArr, name) {
    const timestamp = new Date(Date.now()).toISOString();
    db.collection('names')
        .updateOne(
            {"_id": name._id},
            {$set: {boards: companiesArr, lastChecked: timestamp}},
            {upsert: true},
            (err, results) => {
                if (err) throw err;
                console.log('boards listed for: ', JSON.stringify(name.name));
                return true;
            })
}

let db = null
mongoUtil.connectToServer(function (err, client) {
    if (err) console.log(err);
    db = mongoUtil.getDb();
    run();
});


