const mongoUtil = require( './db' );


let db = null
mongoUtil.connectToServer( function( err, client ) {
    if (err) console.log(err);
    db = mongoUtil.getDb();
    run();
} );

const run = async () => {
    const coll = db.collection('names');

    const list = await coll.aggregate([{$group:{_id:"$name", dups:{$push:"$_id"}, count: {$sum: 1}}},
        {$match:{count: {$gt: 1}}},
        { "$sort": { "count": -1 } },
    ])
        .forEach(function(doc){
        // doc.dups.shift();
        // db.dups.remove({_id : {$in: doc.dups}});
        console.log(doc._id, doc.count);
    })


};
