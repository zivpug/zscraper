const url = 'mongodb://localhost:27017';

const MongoClient = require( 'mongodb' ).MongoClient;


let _db;

module.exports = {

    connectToServer: function( callback ) {
        MongoClient.connect( url,  { useNewUrlParser: true }, function( err, client ) {
            _db  = client.db('directors');
            return callback( err );
        } );
    },

    getDb: function() {
        return _db;
    }
};
