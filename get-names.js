"use strict";

const loadURL = require('./modeles/getURL').loadURL;
const parser = require('./modeles/parser');
const mongoUtil = require( './db' );

// heb letters char list
// removed last letters - 1509, 1507, 1503, 1501, 1498
const ABC = [1488, 1489, 1490, 1491, 1492, 1493, 1494, 1495, 1496, 1497, 1499, 1500, 1502, 1504, 1505, 1506, 1508, 1510, 1511, 1512, 1513, 1514]


const baseUrl = 'http://directors.dundb.co.il/'
// filter=%d7%90&page=1

const nameSelector = 'td.gridDir_colName'
// collect data

/**
 * last so far
 letter:0
 page:243
 * **/

const run = async () => {
    const lastKnown = {
        letter: 0,
        page: 257
    }
    const totalLetters = 22;
    const hasPages = await db.collection('pages').count();
    let lastPageData = (hasPages > 0) ? await db.collection('pages').findOne() : lastKnown;

    let letter =  lastPageData.letter;
    let page = lastPageData.page++;


    while (letter < totalLetters) {
        let html;
        const a = encodeURIComponent(String.fromCharCode(ABC[letter]));
        const url = baseUrl + 'Directors.aspx?filter=' + a + '&page=' + page;
        console.log('loading: ', url)

        try {
            html = await loadURL(url);
        } catch (e) {
            console.log('error: ', e);
            throw e;
        }

        const next = await buildNamesList(html);
        await upsertPage({letter: letter, page: page});
        if (next && next > 0) {
            page++;
        } else {
            page = 1;
            letter++
        }
    }

}

const buildNamesList = async (html) => {
    const names = await parser.testSelector(nameSelector, html);
    const namesArr = [];
    names.each((i, item) => {
        const user = parser.getLink(item);
        namesArr.push(user);
    });
    if (namesArr.length) await insert('names', namesArr);

    return namesArr.length;
}


async function insert(collection, data) {
    db.collection(collection).insertMany(data).then((d) => {
        console.log('inserted successfully: ', data.length);
    })
        .catch((err) => {
            console.log('DB insert error: ', collection, data, err);
            return false;
        });
}

async function upsertPage(data) {
    data.index = 1;
    db.collection('pages')
        §
}

let db = null
mongoUtil.connectToServer( function( err, client ) {
    if (err) console.log(err);
    db = mongoUtil.getDb();
    run();
} );


