const cheerio = require('cheerio');


const testSelector = async (selector, data) => {
    const $ = cheerio.load(data); //  , {decodeEntities: false}
    return $(selector);
}

const getLink = (item) => {
    const href = item.children.find(n => n.type === 'tag' && n.name === 'a');
    const name = href.children[0].data.replace('*', '');
    const url = href.attribs.href;
    return {
        name,
        url
    };
}

const getDirectorBoards = async (tds, type) => {
    const url = tds.find('a').attr('href');
    const name = tds.find('a').text();
    const companyID = url.substr(url.indexOf('duns=')+5)
    const desc = tds.toArray().filter(td=> td.attribs.class==='descriptionCell' )[0].children[0].data;
    const title = tds.toArray().filter(td=> td.attribs.class==='leftMostCell' )[0].children[0].data;
    const obj = {
        companyID,
        name: name,
        url: url,
        desc,
        title,
        type
    }
     return (name) ? obj : null;
}


module.exports = {
    testSelector: testSelector,
    getDirectorBoards: getDirectorBoards,
    getLink: getLink
}
