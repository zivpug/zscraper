const puppeteer = require('puppeteer-extra')
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())


const useProxy = false; // set to use proxy by the settings below.
const proxy = 'socks5://127.0.0.1:9050';
const userAgent = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
    'Mozilla/5.0 (Linux; U; Android 4.1.1; en-us; BroadSign Xpress 1.0.15-6 B- (720) Build/JRO03H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36']

/* Resource types to block from loading, for speed up and less resources */
const blockedResourceTypes = [
    'img',
    'image',
    'iframe',
    'media',
    'font',
    'texttrack',
    'object',
    'beacon',
    'csp_report',
    'imageset'
];

const skippedResources = [
    'quantserve',
    'adzerk',
    'doubleclick',
    'adition',
    'exelator',
    'sharethrough',
    'cdn.api.twitter',
    'google-analytics',
    'googletagmanager',
    'googleadservices',
    'google',
    'googleads',
    'fontawesome',
    'analytics',
    'optimizely',
    'clicktale',
    'mixpanel',
    'zedo',
    'clicksor',
    'tiqcdn',
    'facebook',
    'facebook.com',
    'facebook.net',
    'api.autopilothq.com'
];

// settings


const puppeteerConf = {
    args: [
        "--incognito",
        `--ignore-certificate-errors`,
        '--disable-setuid-sandbox',
        '--no-sandbox',
        '--disable-gpu',
        '--no-first-run',
        '--window-size=1920x1080',
        '--disable-accelerated-2d-canvas=true'
    ],
    headless: true,
    slowMo: 500,
    ignoreHTTPSErrors: true,
    dumpio: true
};

if (useProxy) {
    puppeteerConf.args.push(
        `--proxy-server=` + proxy
    )
}


const loadURL = async (url) => {
    let browser;
    try {
        browser = await puppeteer.launch(puppeteerConf);
        console.log('browser loaded');
    } catch (e) {
        console.log('puppeteer fail', e)
    }

    const randAgent = Math.round(Math.random() * (userAgent.length - 1));

    const page = await browser.newPage();
    await page.setRequestInterception(true);
    // await page.setUserAgent(userAgent[randAgent]);

    try {
        // add header for the navigation requests
        page.on('request', request => {
            const requestUrl = request._url.split('?')[0].split('#')[0];
            // ignore unneeded requests
            if (
                blockedResourceTypes.indexOf(request.resourceType()) !== -1 ||
                skippedResources.some(resource => requestUrl.indexOf(resource) !== -1)
            ) {
                request.abort();
                return;
            }

            // Do nothing in case of non-navigation requests.
            if (!request.isNavigationRequest()) {
                request.continue();
                return;
            }
            // Add a new header for navigation request.
            const headers = request.headers();
            // headers['Access-Control-Allow-Origin'] = '*';
            request.continue({headers});
        });

        // navigate to the website
        const response = await page.goto(url, {
            timeout: 25000,
            waitUntil: 'domcontentloaded', // networkidle0
        });

        if (response._status < 400) {
            try {
                await page.waitFor(1000);
            } catch (e) {
                throw new Error('error on page waitFor:' + e);
            }
            const html = await page.content();
            await browser.close();

            return html;
        }
    } catch (err) {
        // console.error(err);
        // return {err: 'error loading:' + err};
        throw new Error('error loading:' + err);
    }

};

module.exports = {
    loadURL: loadURL
}
